package com.dev.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BoardVO {

	private int seq;
	private int seq_user;
	private String title;
	private String content;
	private String created_at;
	private String updated_at;
	private String visible;
	
	private MemberVO memberVO;

}
