package com.dev.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Paging {
	
	private int startPage;
	private int endPage;
	private int currentPage;
	
	public Paging(int currentPage, int boardCount) {
		this.currentPage = currentPage;
		
		if(boardCount > 100) {
			this.endPage = ((currentPage%10) + 1) * 10;
			this.startPage = this.endPage - 9;
		}else {
			this.startPage = 1 ;
			if(boardCount/10 == 0) {
				this.endPage = 1;
			}else {
				this.endPage = boardCount%10 == 0 ? boardCount/10 : boardCount/10 + 1;
			}
			
		}
	}
	
	

}
