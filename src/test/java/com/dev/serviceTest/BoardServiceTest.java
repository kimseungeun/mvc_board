package com.dev.serviceTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dev.boardTest.BoardTest;
import com.dev.domain.BoardVO;
import com.dev.domain.MemberVO;
import com.dev.service.BoardService;

import lombok.extern.log4j.Log4j;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/**/root-context.xml"})
@Log4j
public class BoardServiceTest {

    @Autowired
    private BoardService service;
    
//    @Test
//    public void testInsert() {
//      BoardVO bvo = new BoardVO();
//   
//      
//      bvo.setTitle("서비스 테스트");
//      bvo.setContent("서비스 설명~");
//      bvo.setSeq_user(1);
//      
//      service.insert_new_writing(bvo);
//  }
//    
//    @Test
//    public void testUpdate() {
//        
//        BoardVO board = new BoardVO();
//        board.setSeq(27);
//        board.setTitle("오늘");
//        board.setContent("오늘내용~");
//        
//        int result = service.update_writing(board);
//        log.info("result : " +result);
//        
//    }
    
//    @Test
//    public void testGETPage() {
//        
//        int seq = 27;
//        
//        log.info("" + service.detail_writing(seq));
//        
//    }

}
