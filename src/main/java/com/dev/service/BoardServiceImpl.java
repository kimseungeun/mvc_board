package com.dev.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.domain.BoardVO;
import com.dev.domain.Paging;
import com.dev.mapper.BoardMapper;

import lombok.AllArgsConstructor;
import lombok.Setter;

@Service("BoardService")
@AllArgsConstructor
public class BoardServiceImpl implements BoardService {
	
	@Setter(onMethod_ = @Autowired)
	public BoardMapper boardMapper;

	@Override
	public List<BoardVO> getBoardlist(int page, String word, String like) {
		page = page == 1 ? 0 : (page-1) * 10;
		return boardMapper.getlist(page, word, like);
	}

	@Override
	public Paging getPaging(int currnetPage, int boardCount) {
		Paging pasing = new Paging(currnetPage, boardCount); 
		return pasing;
	}

	@Override
	public int getBoardlistCount(String word, String like) {
		return boardMapper.getlistCount(word, like);
	}
	
	@Override
    public BoardVO detail_writing(int seq) {
        return boardMapper.detail_writing(seq);
    }  
	 
	@Override
	public void insert_new_writing(BoardVO bvo) {
		boardMapper.insert_new_writing(bvo);
	}
	
	@Override
    public int update_writing(BoardVO board) {
        return boardMapper.update_writing(board);
    }

	@Override
	public void scheduling() {
		boardMapper.updateBoardVisible();
	}
	
	@Override
    public int delete_writing(int seq) {
        return boardMapper.delete_writing(seq);
    }

}
