<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>  
<!DOCTYPE html>
<html>
<head>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<meta charset="UTF-8">
<title>게시판</title>

<style type="text/css">
.btn{
  	display: inline;
    font-size: 10px;
    padding: 6px 12px;
    background-color: #fff;
    border: 1px solid #ddd;
    font-weight: 600;
    width: 140px;
    height: 41px;
    line-height: 39px;
    text-align : center;
    margin-left : 30px;
    cursor : pointer;
}
#deleteBtn{
    background-color: #f3e3e7;
}
</style>

</head>
<body>
	<h1>글 수정하기</h1>

	<form id="updateFrom" action="/board/update_writing" method="post">
		<div class="input_wrap" style="display: none;">
			<label>사용자 번호</label> <input name="seq" readonly="readonly" value='<c:out value="${detailInfo.seq}"/>'>
		</div>
		<div class="input_wrap">
			<label>사용자 번호</label> <input name="seq_user" readonly="readonly" value='<c:out value="${detailInfo.seq_user}"/>'>
		</div>
		<div class="input_wrap">
			<label>제목</label> <input name="title" value='<c:out value="${detailInfo.title}"/>'>
		</div>
		<div class="input_wrap" style="display: flex;">
			<label>내용</label>
			<textarea rows="3" name="content"><c:out value="${detailInfo.content}"/></textarea>
		</div>
		<a class="btn" id="updateBtn">수정</a>
		<a class="btn" id="deleteBtn">삭제</a>
	</form>
	<form id="deleteFrom" action="/board/delete_writing" method="post">
		<input type="hidden" id="seq" name="seq" value='<c:out value="${detailInfo.seq}"/>'>
	</form>

</body>
<script>
	$("#updateBtn").on("click", function(e){
		$("#updateFrom").submit();
	});
	
	$("#deleteBtn").on("click", function(e){
		$("#deleteFrom").submit();
	});
</script>

</html>