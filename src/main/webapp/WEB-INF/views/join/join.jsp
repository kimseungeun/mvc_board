<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<meta charset="UTF-8">
<title>회원가입</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<h1>회원가입</h1>
	<form class="signup">
		<table>
			<tr>
				<td><label for="email" class="form-label">이메일</label></td>
				<td><input type="email" class="form-control" placeholder="example@email.com"></td>
			</tr>
			<tr>
				<td><label for="name" class="form-label">이름</label></td>
				<td><input type="name" class="form-control" placeholder="홍길동"></td>
			</tr>
			<tr>
				<td><label for="password" class="form-label">비밀번호</label></td>
				<td><input type="password" class="form-control" placeholder="대/소문자,숫자,특수기호 포함 8자리"></td>
				
				<td><label for="passwordCheck" class="form-label">비밀번호 확인</label></td>
				<td><input type="passwordCheck" class="form-control" ></td>
			</tr>
		</table>
	</form>
	<div class="btn">
		<button type="submit" class="btn">회원가입</button>
	</div>
</body>

<script>

</script>
</html>