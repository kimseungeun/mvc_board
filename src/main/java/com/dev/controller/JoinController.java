package com.dev.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/board/*")
public class JoinController {

	@RequestMapping(value = "/join")
	public String join() {
		return "join/signup";
	}
}
