<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	<h1>Spring MVC Board</h1>

	<form class="px-4 py-3">
		<table>
			<tbody>
				<tr>
					<td><label for="exampleDropdownFormEmail1" class="form-label">이메일</label></td>
					<td><input type="email" class="form-control"
						id="exampleDropdownFormEmail1" placeholder="email@example.com"
						style="width: width: 240px;"></td>
				</tr>
				<tr>
					<td><label for="exampleDropdownFormPassword1"
						class="form-label">인증코드</label></td>
					<td><input type="password" class="form-control"
						id="exampleDropdownFormPassword1" placeholder=""
						style="width: 240px;"></td>
				</tr>
			</tbody>
		</table>
	</form>
	<div class="btn">
		<button type="code_submit" class="btn btn-primary">인증코드 받기</button>
		<button type="submit" class="btn btn-primary">로그인</button>
	</div>
	<div class="dropdown-divider"></div>
	<a class="dropdown-item" href="#">회원가입</a>

</body>
</html>
