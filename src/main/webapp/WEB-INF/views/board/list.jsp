<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<meta charset="UTF-8">
<title>게시판</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	<h1>게시판</h1>
	<div class="row">
		<div class="col-md-6">
			<button class="btn btn-info" onclick="onclickSearch();">검색</button>
			<button type="button" class="btn btn-success" onclick="onclickNew();">글작성</button>
			<button type="button" class="btn btn-warning">내글보기</button>
			<div class="col-md-6">
				<form class="form-inline" id="frm" action="/ds/board/list">
					<div class="form-group">
						<select class="form-control" name="kind">
							<option value="writer" <c:if test="${kind == 'writer'}">selected</c:if>>작성자</option>
							<option value="title" <c:if test="${kind == 'title'}">selected</c:if>>제목</option>
						</select>
						<input class="form-control" id="word" name="word" value="<c:out value="${word}"/>"></input>
					</div>
					<input type="hidden" id="currentPage" name="page"/>
				</form>
			</div>
		</div>
	</div>
	<br>
	<table class="table table-striped">
		<tr>
			<td>글번호</td>
			<td>제목</td>
			<td>작성자</td>
			<td>작성일</td>
		</tr>
		<c:forEach items="${list}" var="list" >
		<c:set var="i" value="${i+1}" />
			<tr>
				<td><c:out value="${i}" /></td>
				<td><a class="move" href='/board/detail?seq=<c:out value="${list.seq}"/>'><c:out value="${list.title}" /></a></td>
				<td><c:out value="${list.memberVO.name}" /></td>				
				<td><c:out value="${list.updated_at}" /></td>
			</tr>
		</c:forEach>
	</table>
	<div>
		<nav aria-label="...">
			<ul class="pagination">
				<li class="page-item <c:if test="${page.currentPage == 1}">disabled</c:if>" onclick="onclickPagination('previous');">
					<span class="page-link">Previous</span>
				</li>
				<c:forEach var="i" begin="${page.startPage}" end="${page.endPage}">
					<li class="page-item <c:if test="${page.currentPage == i}">active</c:if>">
						<a class="page-link" onclick="onclickPagination('page',${i})">
						<c:out value="${i}"/>
						<c:if test="${page.currentPage == i}">
							<span class="sr-only">(current)</span>
						</c:if>
						</a>
					</li>
				</c:forEach>
				<li class="page-item <c:if test="${page.currentPage == page.endPage}">disabled</c:if>">
					<a class="page-link" onclick="onclickPagination('next')">Next</a>
				</li>
			</ul>
		</nav>
	</div>
</body>
<script>
	$(document).ready(function(){
	    let result='<c:out value="${result}"/>';
	    checkAlert(result);
	   
	});

    function checkAlert(result){
        if(result == ''){
            return;
        }
        if(result == "insert success"){
            alert("등록되었습니다.");
        }
        if(result == "update success"){
            alert("수정되었습니다.");
        }
        if(result == "delete success"){
            alert("삭제되었습니다.");
        }
        
    } 

	function onclickPagination(page,value=null) {
		if(page == 'previous') {
			$('#currentPage').val(${page.currentPage}-1);
		}else if(page == 'next') {
			$('#currentPage').val(${page.currentPage}+1);
		} else {
			$('#currentPage').val(value);
		}
		$('#frm').submit();
	}
	
	function onclickSearch() {
		if($('#word').val() == ""){
			$('#frm').submit();
		}else{
			$('#currentPage').val(${page.currentPage});
			$('#frm').submit();
		}
	}
	
	function onclickNew() {
        location.href = "${pageContext.request.contextPath}/board/new_writing";
	}
</script>
</html>