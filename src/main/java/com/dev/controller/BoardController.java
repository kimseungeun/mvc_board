package com.dev.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.dev.domain.BoardVO;
import com.dev.service.BoardService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@Controller
@RequestMapping("/board/*")
@AllArgsConstructor
@Log4j
public class BoardController {
	 @Autowired
	 private BoardService boardservice;
	   
	
	@RequestMapping(value = "/list")
	public String list(Model model, @RequestParam(value="page", required = false, defaultValue = "1") int page, @RequestParam(value="word",required = false) String word, @RequestParam(value="kind", required = false) String kind) {
		log.info("게시판 목록");
		
		model.addAttribute("list",boardservice.getBoardlist(page, word, kind));
		model.addAttribute("page",boardservice.getPaging(page, boardservice.getBoardlistCount(word, kind)));
		model.addAttribute("kind",kind);
		model.addAttribute("word",word);
		
		return "board/list";
	}
	
	
	// 새 글 등록 화면	
	@GetMapping("/new_writing")
	// = @RequestMapping(value = "/new_writing", method=RequestMethod.GET)
	public void new_writing_get() {
		log.info("새 글 등록 화면");
	}
	
	// 새 글 등록하기
	@PostMapping("/new_writing")
	public String new_writing_post(BoardVO board, RedirectAttributes rattr) {
		log.info("BoardVO : "+ board);
		boardservice.insert_new_writing(board);
		
		rattr.addFlashAttribute("result", "insert success");
		
		return "redirect:/board/list";
	}
	
	@GetMapping("/detail")
    public void detail_get(int seq, Model model) {
        model.addAttribute("detailInfo", boardservice.detail_writing(seq));
        
    }
	
	@PostMapping("/update_writing")
    public String update_writing_post(BoardVO board, RedirectAttributes rattr) {
		boardservice.update_writing(board);
        
        rattr.addFlashAttribute("result", "update success");
        
        return "redirect:/board/list";
    }
	
    @PostMapping("/delete_writing")
    public String delete_writing_post(int seq, RedirectAttributes rattr) {
        boardservice.delete_writing(seq);
        
        rattr.addFlashAttribute("result", "delete success");
        
        return "redirect:/board/list";
    }
	
	

}
