package com.dev.boardTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dev.domain.BoardVO;
import com.dev.domain.MemberVO;
import com.dev.mapper.BoardMapper;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/**/root-context.xml"})
@Log4j
public class BoardTest {
	
	@Setter(onMethod_ = @Autowired)
	public BoardMapper boardmapper;
	
//	@Test
//	public void getlistTest() {
//		boardmapper.getlist(0, null, null).forEach(board -> log.info(board.getSeq_user()));
//	}
	
//	@Test
//	public void getlistCountTest() {
//		BoardVO vo = new BoardVO();
//		System.out.println(vo.getMemberVO().getSeq());
//	}
//	
//	 @Test
//     public void testInsert() {
//         
//         BoardVO bvo = new BoardVO();
//         
//         bvo.setTitle("제목");
//         bvo.setContent("설명~");
//         bvo.setSeq_user(1);
//         
//         boardmapper.insert_new_writing(bvo);
//     }
	
	@Test
    public void testUpdate() {
        
        BoardVO board = new BoardVO();
        board.setSeq(3);
        board.setTitle("수정 제목~~~");
        board.setContent("수정 내용~~~~~");
        
        int result = boardmapper.update_writing(board);
        log.info("result : " +result);
        
    }


}
