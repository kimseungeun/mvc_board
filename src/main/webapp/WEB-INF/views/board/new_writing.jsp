<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<meta charset="UTF-8">
<title>게시판</title>
</head>
<body>
	<h1>새 글 등록하기</h1>

	<form action="/board/new_writing" method="post">
		<div class="input_wrap">
			<label>사용자 번호</label> <input name="seq_user" value="1">
		</div>
		<div class="input_wrap">
			<label>제목</label> <input name="title">
		</div>
		<div class="input_wrap" style="display: flex;">
			<label>내용</label>
			<textarea rows="3" name="content"></textarea>
		</div>
		<button class="btn">등록</button>
	</form>

</body>
</html>