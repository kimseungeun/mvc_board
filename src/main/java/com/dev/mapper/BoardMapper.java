package com.dev.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dev.domain.BoardVO;

public interface BoardMapper {
	
	public List<BoardVO> getlist(@Param("page") int page, @Param("word") String word, @Param("like") String like);
	
	public int getlistCount(@Param("word") String word, @Param("like") String like);
	
	public List<BoardVO> search(String woard);
	
	public BoardVO detail_writing(int seq);
	
	public void insert_new_writing(BoardVO board);
	
	public int update_writing(BoardVO board);
	
	public void updateBoardVisible();
	
	public int delete_writing(int seq);

}
