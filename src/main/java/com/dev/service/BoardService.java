package com.dev.service;

import java.util.List;

import com.dev.domain.BoardVO;
import com.dev.domain.Paging;

public interface BoardService {
	
	public List<BoardVO> getBoardlist(int page, String word, String like);
	
	public Paging getPaging(int currnetPage, int boardCount);
	
	public int getBoardlistCount(String word, String like);
	
	public BoardVO detail_writing(int seq);
	
	public void insert_new_writing(BoardVO board);
	
	public int update_writing(BoardVO board);
	
	public void scheduling();
	
	public int delete_writing(int seq);
	
}
