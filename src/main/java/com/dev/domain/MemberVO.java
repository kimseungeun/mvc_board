package com.dev.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberVO {
	
	private int seq;
	private String name;
	private String email;
	private String created_at;

}
